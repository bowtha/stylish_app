import React, { useState, useEffect } from 'react';
import { Container, Text, Header, Left, Right,  Body } from 'native-base';
import { StyleSheet, View, Image, ScrollView, FlatList, TouchableOpacity, StatusBar } from 'react-native';
import { ActivityIndicator, Colors, Card } from 'react-native-paper';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faSearch, faMapMarkerAlt, faCloud, faSyncAlt, faUserCircle } from '@fortawesome/free-solid-svg-icons'
import { getRandomCollection,  getTempByLatLong } from '../api';
import Geolocation from '@react-native-community/geolocation';
import { useSelector } from 'react-redux';

const HomeScreen = ({ navigation }) => {

    const token = useSelector(state => state.login.token)

    const [location,setLocation] = useState("")
    const [collection, setCollection] = useState([])
    const [temp, setTemp] = useState(0)

    const randCollection = async () => {
        await getRandomCollection()
            .then(response => {
                setCollection(response.data)
            })
            .catch(err => console.log(err))
    }

    const getTemp = () => {
        Geolocation.getCurrentPosition(info => {
            const lat = info.coords.latitude
            const lon = info.coords.longitude
            getTempByLatLong(lat, lon)
                .then(response => {
                    calTemp(response.data)
                    setLocation(response.data.name)
                })
                .catch(err => console.log(err))
        });
    }

    const calTemp = async (data) => {
        const c = Math.floor((Number(data.main.temp) - 273.15))
        await setTemp(c)
    }

    const reload = () => {
        randCollection()
        getTemp()
    }

    useEffect(() => {
        randCollection()
        getTemp()
    }, [])

    return (
        <Container contentContainerStyle={{ flexGrow: 1 }} >
            <StatusBar translucent hidden/>
            <Header style={styles.header}>
                <Left style={{ flex: 1 }}>
                    {
                        token === null ?
                            <TouchableOpacity style={{ marginLeft: 8 }} onPress={() => navigation.navigate('Login')}>
                                <FontAwesomeIcon icon={faUserCircle} size={24} />
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={{ marginLeft: 8 }} onPress={() => navigation.navigate('Profile')}>
                                <FontAwesomeIcon icon={faUserCircle} size={24} />
                            </TouchableOpacity>
                    }
                </Left>
                <Body style={{ flex: 2 }} >
                    <Text style={{ fontSize: 24, alignSelf: 'center' }}>STYLISH</Text>
                </Body>
                <Right style={{ flex: 1 }}>
                    {
                        token === null ?
                            <TouchableOpacity style={{ marginLeft: 8 }} onPress={() => navigation.navigate('Login')}>
                                <FontAwesomeIcon icon={faSearch} size={24} />
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={{ marginRight: 8 }} onPress={() => navigation.navigate('Search')}>
                                <FontAwesomeIcon icon={faSearch} size={24} />
                            </TouchableOpacity>
                    }
                </Right>
            </Header>
            {
                collection === [] || temp === null ?
                    <ActivityIndicator animating={true} color={Colors.red800} />
                    :
                    <View style={styles.background} >
                        <ScrollView
                            horizontal={true}
                            style={styles.container}
                        >
                            <FlatList
                                data={collection}
                                renderItem={img => (
                                    <Card style={styles.image} >
                                        <Card.Cover source={{ uri: `${img.item.Image}` }} />
                                    </Card>
                                )}
                                numColumns={3}
                                keyExtractor={(img, index) => index}
                            />
                        </ScrollView>
                        <View style={styles.blockTemp}>
                            <View style={{ flexDirection: 'row', marginTop: '8%', marginLeft: '3%' }}>
                                <FontAwesomeIcon icon={faMapMarkerAlt} color={"#FCFAF1"} size={24} />
                                <Text style={styles.icontemp}>{"Today, " + location}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', flex: 1, marginLeft: '8%' }}>
                                <Text style={[styles.temp, { flex: 1 }]}>{temp}</Text>
                                <View style={{ flex: 2 }}>
                                    <FontAwesomeIcon icon={faCloud} color={"#FCFAF1"} size={32} style={{ marginTop: 8, marginLeft: 16 }} />
                                </View>
                                <TouchableOpacity style={{ flex: 1, alignSelf: 'center', marginTop: '-14%', marginRight: 32 }} onPress={() => reload()}>
                                    <View style={{ backgroundColor: '#FCFAF1', borderRadius: 50, padding: 8, width: 48 }}>
                                        <FontAwesomeIcon icon={faSyncAlt} size={32} />
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
            }
        </Container>
    )
}
export default HomeScreen

const styles = StyleSheet.create({
    background: {
        flex: 1,
        backgroundColor: 'white',
        height: '100%'
    },
    container: {
        margin: 24,
        marginTop: 8,
        height: '80%',
    },
    blockTemp: {
        backgroundColor: 'gray',
        height: '25%',
        paddingLeft: '8%'
    },
    image: {
        flex: 1,
        width: 150,
        height: 185,
        borderRadius: 10,
        elevation: 8,
        margin: 8,
    },
    header: {
        backgroundColor: 'white',
        paddingTop:20
    },
    picker: {
        color: '#EA5E5E',
        backgroundColor: '#EA5E5E'
    },

    icontemp: {
        color: '#FCFAF1',
        fontSize: 20,
        marginLeft: 8
    },
    temp: {
        fontSize: 56,
        color: '#FCFAF1',
    }
})
