import React, { useState, useEffect, useCallback } from 'react';
import { Container, Text, Header, Left, Right, Button, Title, Icon, Body, Content } from 'native-base';
import { StyleSheet, View, Image, ScrollView, FlatList, TouchableOpacity, RefreshControl, StatusBar } from 'react-native';
import { ActivityIndicator, Colors, Card } from 'react-native-paper';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faSearch, faMapMarkerAlt, faCloud, faSyncAlt, faUserCircle, faHome, faArrowLeft } from '@fortawesome/free-solid-svg-icons'
import { getRandomCollection, host, getCollectionByID } from '../api';
import { useSelector } from 'react-redux';

const ViewCollectionScreen = ({ route, navigation }) => {

    const token = useSelector(state => state.login.token)
    const [data, setData] = useState(null)
    const [collection, setCollection] = useState([])
    const [refreshing, setRefreshing] = useState(false);

    const collectionByID = async () => {
        const id = route.params.collection_id

        await getCollectionByID(token, id)
            .then(response => {
                setCollection(response.data)
            })
            .catch(err => console.log(err))
    }

    useEffect(() => {
        collectionByID()
    }, [])


    if (collection.length > 0) {
        return (
            <Container contentContainerStyle={{ flexGrow: 1 }} >
                <StatusBar backgroundColor="gray" />
                <Header style={styles.header}>
                    <Left style={{ flex: 1 }}>
                        <TouchableOpacity style={{ marginLeft: 8 }} onPress={() => navigation.goBack()}>
                            <FontAwesomeIcon icon={faArrowLeft} size={24} />
                        </TouchableOpacity>

                    </Left>
                    <Body style={{ flex: 2 }} >
                        <Text style={{ fontSize: 24, alignSelf: 'center' }}>STYLISH</Text>
                    </Body>
                    <Right style={{ flex: 1 }}>
                        <TouchableOpacity style={{ marginLeft: 8 }} onPress={() => navigation.navigate('Profile')}>
                            <FontAwesomeIcon icon={faUserCircle} size={24} />
                        </TouchableOpacity>
                        <TouchableOpacity style={{ marginRight: 8, marginLeft: 16 }} onPress={() => navigation.navigate('Home')}>
                            <FontAwesomeIcon icon={faHome} size={24} />
                        </TouchableOpacity>
                    </Right>
                </Header>
                {
                    collection === [] ?
                        <ActivityIndicator animating={true} color={Colors.red800} />
                        :
                        <View style={styles.background} >
                            <ScrollView
                                style={styles.container}
                            >
                                <FlatList
                                    data={collection}
                                    renderItem={img => (
                                        <Card style={styles.image} >
                                            <Card.Cover source={{ uri: `${img.item.Image}` }} />
                                        </Card>
                                    )}
                                    numColumns={2}
                                    keyExtractor={(img, index) => index}
                                />
                            </ScrollView>
                        </View>
                }
            </Container>
        )
    } else {
        return (
            <Container></Container>
        )
    }
}
export default ViewCollectionScreen

const styles = StyleSheet.create({
    background: {
        flex: 1,
        backgroundColor: 'white',
        height: '100%'
    },
    container: {
        margin: 24,
        marginTop: 8,
        height: '80%',
    },
    blockTemp: {
        backgroundColor: 'gray',
        height: '25%',
        paddingLeft: '8%'
    },
    image: {
        flex: 1,
        width: 150,
        height: 185,
        borderRadius: 10,
        elevation: 8,
        margin: 8,
    },
    header: {
        backgroundColor: 'white',
        paddingTop:20
    },
    picker: {
        color: '#EA5E5E',
        backgroundColor: '#EA5E5E'
    },

    icontemp: {
        color: '#FCFAF1',
        fontSize: 20,
        marginLeft: 8
    },
    temp: {
        fontSize: 56,
        color: '#FCFAF1',
    }
})
