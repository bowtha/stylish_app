import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Text, View, ImageBackground, KeyboardAvoidingView } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { TextInput, Button, HelperText } from 'react-native-paper'
import { login } from '../api/index'
import { isLogin } from '../actions/loginAction';
import { useDispatch } from 'react-redux';



const LoginForm = ({ navigation }) => {

    const dispatch = useDispatch()

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const [isError, setIsError] = useState(false)
    const [match, setMatch] = useState(false)

    const [onlogin, setOnlogin] = useState(false)
    const [isMin, setIsmin] = useState(false)

    const onLogin = () => {
        if (email == "" || password == "") {
            setOnlogin(true)
        }
        else {
            const data = {
                "Email": email,
                "Password": password,
            }
            login(data)
                .then((response) => {
                    console.log(response.data.token)
                    dispatch(isLogin(response.data.token))
                    navigation.navigate("Profile")
                })
                .catch(err => {
                    console.log(err)
                    setIsError(true)
                })
        }
    };

    return (
        <ImageBackground
            source={{ uri: 'https://i.pinimg.com/474x/c2/f8/f2/c2f8f2254e654a354958cd7dab55dff8.jpg' }}
            style={styles.background}
        >
            <KeyboardAvoidingView behavior="padding">
                <TouchableOpacity onPress={() => navigation.navigate('Login')} style={styles.container} >
                    <FontAwesomeIcon icon={faArrowLeft} />
                </TouchableOpacity>
                <Text style={styles.header}>Welcome back.</Text>
                <View style={styles.content}>
                    <TextInput
                        label="Email"
                        returnKeyType="next"
                        value={email}
                        onChangeText={text => {
                            setEmail(text)
                        }}
                        error={isError}
                        autoCapitalize="none"
                        autoCompleteType="email"
                        textContentType="emailAddress"
                        keyboardType="email-address"
                        style={styles.input}
                    />

                    <TextInput
                        label="Password"
                        value={password}
                        onChangeText={text => {
                            setPassword(text)
                        }}
                        error={isError}
                        style={styles.input}
                        secureTextEntry
                    />
                    <HelperText
                        type="error"
                        visible={isError}
                    >
                        Incorrect E-mail And/or Password
                    </HelperText>
                    <Button mode="contained"
                        disabled={onlogin}
                        style={styles.button}
                        onPress={() => onLogin()}
                    >Login</Button>
                    <View style={styles.row}>
                        <Text style={{ textAlign: 'center' }}>Don’t have an account? </Text>
                        <TouchableOpacity onPress={() => navigation.navigate('Register')}>
                            <Text style={styles.link}>Sign up</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </KeyboardAvoidingView>
        </ImageBackground>
    );
};
export default LoginForm;

const styles = StyleSheet.create({
    forgotPassword: {
        width: '100%',
        alignItems: 'flex-end',
        marginBottom: 24,
        marginTop: 4
    },
    row: {
        flexDirection: 'row',
        marginTop: 4,
        alignSelf: 'center'
    },
    link: {
        fontWeight: 'bold',
        textAlign: 'center',
    },
    header: {
        fontSize: 26,
        fontWeight: 'bold',
        paddingVertical: 14,
        textAlign: 'center',
        marginTop: 16
    },
    background: {
        flex: 1,
        width: '100%',
    },
    container: {
        margin: 32
    },
    content: {
        width: '80%',
        marginLeft: '10%',
        marginTop: 32
    },
    input: {
        marginTop: 16,
        height: 48
    },
    button: {
        width: '40%',
        marginLeft: '30%',
        elevation: 15,
        backgroundColor: 'black',
        marginTop: 30
    },
});

