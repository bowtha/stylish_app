import React, { useState } from 'react';
import { Container, Text, Header, Left, Right, Button, Body, Content, ListItem, Radio } from 'native-base';
import { StyleSheet, View, FlatList, TouchableOpacity } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import {  faUserCircle, faHome } from '@fortawesome/free-solid-svg-icons'
import { List, Searchbar, Dialog, Card } from 'react-native-paper';
import { getCollectionByFilter, getItemsByFilter } from '../api';
import { useSelector } from 'react-redux';

const SearchScreen = ({ navigation }) => {

    const token = useSelector(state => state.login.token)



    const [expanded, setExpanded] = useState(false)
    const [visibleLocation, setVisibleLocation] = useState(false)
    const [visibleStyle, setVisibleStyle] = useState(false)
    const [visibleSearch, setVisibleSearch] = useState(false)
    const [visibleGender, setVisibleGender] = useState(false)
    const [visibleSeasons, setVisibleSeasons] = useState(false)

    const [chooseCollection, setChooseCollection] = useState(true)
    const [chooseItems, setChooseItems] = useState(false)

    const [chooseLocation, setChooseLocation] = useState("%")
    const [chooseAllLocation, setChooseAllLocation] = useState(true)
    const [chooseTH, setChooseTH] = useState(false)
    const [chooseNYC, setChooseNYC] = useState(false)
    const [choosePS, setChoosePS] = useState(false)
    const [chooseLD, setChooseLD] = useState(false)
    const [chooseTK, setChooseTK] = useState(false)
    const [chooseKR, setChooseKR] = useState(false)

    const [chooseStyle, setChooseStyle] = useState("%")
    const [chooseAllStyle, setChooseAllStyle] = useState(true)
    const [chooseOffice, setChooseOffice] = useState(false)
    const [chooseCasual, setChooseCasual] = useState(false)
    const [chooseParty, setChooseParty] = useState(false)
    const [chooseCeremony, setChooseCeremony] = useState(false)
    const [chooseSport, setChooseSport] = useState(false)
    const [chooseRelax, setChooseRelax] = useState(false)

    const [chooseGender, setChooseGender] = useState("%")
    const [chooseAllGender, setChooseAllGender] = useState(true)
    const [chooseMen, setChooseMen] = useState(false)
    const [chooseWomen, setChooseWomen] = useState(false)

    const [chooseSeasons, setChooseSeasons] = useState("%")
    const [chooseAllSeasons, setChooseAllSeasons] = useState(true)
    const [chooseSpring, setChooseSpring] = useState(false)
    const [chooseWinter, setChooseWinter] = useState(false)
    const [chooseAutumn, setChooseAutumn] = useState(false)
    const [chooseSummer, setChooseSummer] = useState(false)
    const [chooseRainy, setChooseRainy] = useState(false)

    const [search, setSearch] = useState("")

    const [dataFilter, setDataFilter] = useState(null)

    const showDialogLocation = () => {
        setVisibleLocation(true)
    }
    const showDialogStyle = () => {
        setVisibleStyle(true)
    }
    const showDialogSearch = () => {
        setVisibleSearch(true)
    }
    const showDialogGender = () => {
        setVisibleGender(true)
    }
    const showDialogSeasons = () => {
        setVisibleSeasons(true)
    }
    const hideDialog = () => {
        setVisibleLocation(false)
        setVisibleStyle(false)
        setVisibleSearch(false)
        setVisibleGender(false)
        setVisibleSeasons(false)
        setExpanded(false)
    }
    const handlePress = () => {
        setExpanded(!expanded)
    }

    const onChangeSearch = async (text) => {
        await (setSearch(text))
        onDone()
    }

    const onDone = () => {
        const Data = {
            "Location": chooseLocation,
            "Gender": chooseGender,
            "Style": chooseStyle,
            "Seasons": chooseSeasons,
        }
        if (chooseCollection === true) {
            getCollectionByFilter(token, search, Data)
                .then(response => {
                    console.log('collection', response.data)
                    setDataFilter(response.data)
                })
                .catch(err => console.log(err))
        }
        else {
            getItemsByFilter(token, search, Data)
                .then(response => {
                    console.log('item', response.data)
                    setDataFilter(response.data)
                })
                .catch(err => console.log(err))
        }
    }


    return (
        <Container contentContainerStyle={{ flexGrow: 1 }}>
            <Header style={styles.header}>
                <Left style={{ flex: 1 }}>
                    <TouchableOpacity style={{ marginLeft: 8 }} onPress={() => navigation.navigate('Profile')}>
                        <FontAwesomeIcon icon={faUserCircle} size={24} />
                    </TouchableOpacity>
                </Left>
                <Body style={{ flex: 2 }} >
                    <Text style={{ fontSize: 24, alignSelf: 'center' }}>STYLISH</Text>
                </Body>
                <Right style={{ flex: 1 }}>
                    <TouchableOpacity style={{ marginRight: 8 }} onPress={() => navigation.navigate('Home')}>
                        <FontAwesomeIcon icon={faHome} size={24} />
                    </TouchableOpacity>
                </Right>
            </Header>
            <Content style={styles.background}>
                <View style={{ flexDirection: 'row', width: '100%' }}>
                    <View style={{ width: '60%' }}>
                        <Searchbar
                            placeholder="Search"
                            style={styles.input}
                            onChangeText={(text) => onChangeSearch(text)}
                            value={search}
                        />
                    </View>
                    <View style={{ width: '30%', marginLeft: '5%', borderRadius: 10, borderWidth: 1, borderColor: 'gray' }}>
                        <List.Accordion
                            title="filter"
                            expanded={expanded}
                            onPress={() => handlePress()}
                        >
                            <List.Item title="Search By" onPress={() => showDialogSearch()} />
                            <List.Item title="Location" onPress={() => showDialogLocation()} />
                            <List.Item title="Seasons" onPress={() => showDialogSeasons()} />
                            <List.Item title="Style" onPress={() => showDialogStyle()} />
                            <List.Item title="Gender" onPress={() => showDialogGender()} />
                        </List.Accordion>
                    </View>
                </View>
                <FlatList
                    style={{ marginTop: '5%', flex: 1 }}
                    data={dataFilter}
                    renderItem={item => (
                        <Card
                            style={styles.image}
                            onPress={() => navigation.navigate('ViewCollection', {
                                collection_id: item.item.CollectionID
                            })}
                        >
                            <Card.Cover source={{ uri: `${item.item.Image}` }} />
                        </Card>
                    )}
                    numColumns={2}
                    keyExtractor={(img, index) => index}
                />
            </Content>
            <Dialog
                visible={visibleSearch}
                onDismiss={() => hideDialog()}>
                <Dialog.Content>
                    <View>
                        <ListItem>
                            <Left>
                                <Text>Collection</Text>
                            </Left>
                            <Right>
                                <Radio
                                    disabled={chooseCollection}
                                    selectedColor={"#5cb85c"}
                                    selected={chooseCollection}
                                    onPress={() => {
                                        setChooseCollection(!chooseCollection)
                                        setChooseItems(chooseCollection)
                                    }}
                                />
                            </Right>
                        </ListItem>
                        <ListItem>
                            <Left>
                                <Text>Items</Text>
                            </Left>
                            <Right>
                                <Radio
                                    disabled={chooseItems}
                                    selectedColor={"#5cb85c"}
                                    selected={chooseItems}
                                    onPress={() => {
                                        setChooseItems(!chooseItems)
                                        setChooseCollection(chooseItems)
                                    }}
                                />
                            </Right>
                        </ListItem>
                    </View>
                </Dialog.Content>
                <Dialog.Actions>
                    <Button onPress={() => hideDialog()}>
                        <Text>Done</Text>
                    </Button>
                </Dialog.Actions>
            </Dialog>
            <Dialog
                visible={visibleLocation}
                onDismiss={() => hideDialog()}>
                <Dialog.Content>
                    <View>
                        <ListItem>
                            <Left>
                                <Text>All Loacation</Text>
                            </Left>
                            <Right>
                                <Radio
                                    disabled={chooseAllLocation}
                                    selectedColor={"#5cb85c"}
                                    selected={chooseAllLocation}
                                    onPress={() => {
                                        setChooseAllLocation(!chooseAllLocation)
                                        setChooseTH(chooseAllLocation)
                                        setChooseNYC(chooseAllLocation)
                                        setChoosePS(chooseAllLocation)
                                        setChooseLD(chooseAllLocation)
                                        setChooseTK(chooseAllLocation)
                                        setChooseKR(chooseAllLocation)
                                        setChooseLocation("%")
                                        // setSeasons("%")
                                    }}
                                />
                            </Right>
                        </ListItem>
                        <ListItem>
                            <Left>
                                <Text>Thailand</Text>
                            </Left>
                            <Right>
                                <Radio
                                    disabled={chooseTH}
                                    selectedColor={"#5cb85c"}
                                    selected={chooseTH}
                                    onPress={() => {
                                        setChooseAllLocation(chooseTH)
                                        setChooseTH(!chooseTH)
                                        setChooseNYC(chooseTH)
                                        setChoosePS(chooseTH)
                                        setChooseLD(chooseTH)
                                        setChooseTK(chooseTH)
                                        setChooseKR(chooseTH)
                                        setChooseLocation("TH")
                                        // setSeasons("rainy")
                                    }}
                                />
                            </Right>
                        </ListItem>
                        <ListItem>
                            <Left>
                                <Text>NYC</Text>
                            </Left>
                            <Right>
                                <Radio
                                    disabled={chooseNYC}
                                    selectedColor={"#5cb85c"}
                                    selected={chooseNYC}
                                    onPress={() => {
                                        setChooseAllLocation(chooseNYC)
                                        setChooseTH(chooseNYC)
                                        setChooseNYC(!chooseNYC)
                                        setChoosePS(chooseNYC)
                                        setChooseLD(chooseNYC)
                                        setChooseTK(chooseNYC)
                                        setChooseKR(chooseNYC)
                                        setChooseLocation("NYC")
                                        // setSeasons("spring")
                                    }}
                                />
                            </Right>
                        </ListItem>
                        <ListItem>
                            <Left>
                                <Text>Paris</Text>
                            </Left>
                            <Right>
                                <Radio
                                    disabled={choosePS}
                                    selectedColor={"#5cb85c"}
                                    selected={choosePS}
                                    onPress={() => {
                                        setChooseAllLocation(choosePS)
                                        setChooseTH(choosePS)
                                        setChooseNYC(choosePS)
                                        setChoosePS(!choosePS)
                                        setChooseLD(choosePS)
                                        setChooseTK(choosePS)
                                        setChooseKR(choosePS)
                                        setChooseLocation("PS")
                                        // setSeasons("spring")
                                    }}
                                />
                            </Right>
                        </ListItem>
                        <ListItem>
                            <Left>
                                <Text>London</Text>
                            </Left>
                            <Right>
                                <Radio
                                    disabled={chooseLD}
                                    selectedColor={"#5cb85c"}
                                    selected={chooseLD}
                                    onPress={() => {
                                        setChooseAllLocation(chooseLD)
                                        setChooseTH(chooseLD)
                                        setChooseNYC(chooseLD)
                                        setChoosePS(chooseLD)
                                        setChooseLD(!chooseLD)
                                        setChooseTK(chooseLD)
                                        setChooseKR(chooseLD)
                                        setChooseLocation("LD")
                                        // setSeasons("spring")
                                    }}
                                />
                            </Right>
                        </ListItem>
                        <ListItem>
                            <Left>
                                <Text>Tokyo</Text>
                            </Left>
                            <Right>
                                <Radio
                                    disabled={chooseTK}
                                    selectedColor={"#5cb85c"}
                                    selected={chooseTK}
                                    onPress={() => {
                                        setChooseAllLocation(chooseTK)
                                        setChooseTH(chooseTK)
                                        setChooseNYC(chooseTK)
                                        setChoosePS(chooseTK)
                                        setChooseLD(chooseTK)
                                        setChooseTK(!chooseTK)
                                        setChooseKR(chooseTK)
                                        setChooseLocation("TK")
                                        // setSeasons("spring")
                                    }}
                                />
                            </Right>
                        </ListItem>
                        <ListItem>
                            <Left>
                                <Text>Korea</Text>
                            </Left>
                            <Right>
                                <Radio
                                    disabled={chooseKR}
                                    selectedColor={"#5cb85c"}
                                    selected={chooseKR}
                                    onPress={() => {
                                        setChooseAllLocation(chooseKR)
                                        setChooseTH(chooseKR)
                                        setChooseNYC(chooseKR)
                                        setChoosePS(chooseKR)
                                        setChooseLD(chooseKR)
                                        setChooseTK(chooseKR)
                                        setChooseKR(!chooseKR)
                                        setChooseLocation("KR")
                                        // setSeasons("spring")
                                    }}
                                />
                            </Right>
                        </ListItem>
                    </View>
                </Dialog.Content>
                <Dialog.Actions>
                    <Button onPress={() => {
                        hideDialog()
                        onDone()
                    }}>
                        <Text>Done</Text>
                    </Button>
                </Dialog.Actions>
            </Dialog>
            {/* dgkshagladblveytgrgkrys  */}
            <Dialog
                visible={visibleSeasons}
                onDismiss={() => hideDialog()}>
                <Dialog.Content>
                    <View>
                        <ListItem>
                            <Left>
                                <Text>All Seasons</Text>
                            </Left>
                            <Right>
                                <Radio
                                    disabled={chooseAllSeasons}
                                    selectedColor={"#5cb85c"}
                                    selected={chooseAllSeasons}
                                    onPress={() => {
                                        setChooseAllSeasons(!chooseAllSeasons)
                                        setChooseAutumn(chooseAllSeasons)
                                        setChooseWinter(chooseAllSeasons)
                                        setChooseRainy(chooseAllSeasons)
                                        setChooseSpring(chooseAllSeasons)
                                        setChooseSummer(chooseAllSeasons)
                                        setChooseSeasons("%")
                                    }}
                                />
                            </Right>
                        </ListItem>
                        <ListItem>
                            <Left>
                                <Text>Autumn</Text>
                            </Left>
                            <Right>
                                <Radio
                                    disabled={chooseAutumn}
                                    selectedColor={"#5cb85c"}
                                    selected={chooseMen}
                                    onPress={() => {
                                        setChooseAllSeasons(chooseAutumn)
                                        setChooseAutumn(!chooseAutumn)
                                        setChooseWinter(chooseAutumn)
                                        setChooseRainy(chooseAutumn)
                                        setChooseSpring(chooseAutumn)
                                        setChooseSummer(chooseAutumn)
                                        setChooseSeasons("autumn")
                                    }}
                                />
                            </Right>
                        </ListItem>
                        <ListItem>
                            <Left>
                                <Text>Winter</Text>
                            </Left>
                            <Right>
                                <Radio
                                    disabled={chooseWinter}
                                    selectedColor={"#5cb85c"}
                                    selected={chooseWinter}
                                    onPress={() => {
                                        setChooseAllSeasons(chooseWinter)
                                        setChooseAutumn(chooseWinter)
                                        setChooseWinter(!chooseWinter)
                                        setChooseRainy(chooseWinter)
                                        setChooseSpring(chooseWinter)
                                        setChooseSummer(chooseWinter)
                                        setChooseSeasons("winter")
                                    }}
                                />
                            </Right>
                        </ListItem>
                        <ListItem>
                            <Left>
                                <Text>Rainy</Text>
                            </Left>
                            <Right>
                                <Radio
                                    disabled={chooseRainy}
                                    selectedColor={"#5cb85c"}
                                    selected={chooseRainy}
                                    onPress={() => {
                                        setChooseAllSeasons(chooseRainy)
                                        setChooseAutumn(chooseRainy)
                                        setChooseWinter(chooseRainy)
                                        setChooseRainy(!chooseRainy)
                                        setChooseSpring(chooseRainy)
                                        setChooseSummer(chooseRainy)
                                        setChooseSeasons("rainy")
                                    }}
                                />
                            </Right>
                        </ListItem>
                        <ListItem>
                            <Left>
                                <Text>Spring</Text>
                            </Left>
                            <Right>
                                <Radio
                                    disabled={chooseSpring}
                                    selectedColor={"#5cb85c"}
                                    selected={chooseSpring}
                                    onPress={() => {
                                        setChooseAllSeasons(chooseSpring)
                                        setChooseAutumn(chooseSpring)
                                        setChooseWinter(chooseSpring)
                                        setChooseRainy(chooseSpring)
                                        setChooseSpring(!chooseSpring)
                                        setChooseSummer(chooseSpring)
                                        setChooseSeasons("spring")
                                    }}
                                />
                            </Right>
                        </ListItem>
                        <ListItem>
                            <Left>
                                <Text>Summer</Text>
                            </Left>
                            <Right>
                                <Radio
                                    disabled={chooseSummer}
                                    selectedColor={"#5cb85c"}
                                    selected={chooseSummer}
                                    onPress={() => {
                                        setChooseAllSeasons(chooseSummer)
                                        setChooseAutumn(chooseSummer)
                                        setChooseWinter(chooseSummer)
                                        setChooseRainy(chooseSummer)
                                        setChooseSpring(chooseSummer)
                                        setChooseSummer(!chooseSummer)
                                        setChooseSeasons("summer")
                                    }}
                                />
                            </Right>
                        </ListItem>
                    </View>
                </Dialog.Content>
                <Dialog.Actions>
                    <Button onPress={() => {
                        hideDialog()
                        onDone()
                    }}>
                        <Text>Done</Text>
                    </Button>
                </Dialog.Actions>
            </Dialog>
            <Dialog
                visible={visibleStyle}
                onDismiss={() => hideDialog()}>
                <Dialog.Content>
                    <View>
                        <ListItem>
                            <Left>
                                <Text>All Style</Text>
                            </Left>
                            <Right>
                                <Radio
                                    disabled={chooseAllStyle}
                                    selectedColor={"#5cb85c"}
                                    selected={chooseAllStyle}
                                    onPress={() => {
                                        setChooseAllStyle(!chooseAllStyle)
                                        setChooseOffice(chooseAllStyle)
                                        setChooseCasual(chooseAllStyle)
                                        setChooseParty(chooseAllStyle)
                                        setChooseCeremony(chooseAllStyle)
                                        setChooseSport(chooseAllStyle)
                                        setChooseRelax(chooseAllStyle)
                                        setChooseStyle("%")
                                    }}
                                />
                            </Right>
                        </ListItem>
                        <ListItem>
                            <Left>
                                <Text>Office</Text>
                            </Left>
                            <Right>
                                <Radio
                                    disabled={chooseOffice}
                                    selectedColor={"#5cb85c"}
                                    selected={chooseOffice}
                                    onPress={() => {
                                        setChooseAllStyle(chooseOffice)
                                        setChooseOffice(!chooseOffice)
                                        setChooseCasual(chooseOffice)
                                        setChooseParty(chooseOffice)
                                        setChooseCeremony(chooseOffice)
                                        setChooseSport(chooseOffice)
                                        setChooseRelax(chooseOffice)
                                        setChooseStyle("office")
                                    }}
                                />
                            </Right>
                        </ListItem>
                        <ListItem>
                            <Left>
                                <Text>Casual</Text>
                            </Left>
                            <Right>
                                <Radio
                                    disabled={chooseCasual}
                                    selectedColor={"#5cb85c"}
                                    selected={chooseCasual}
                                    onPress={() => {
                                        setChooseAllStyle(chooseCasual)
                                        setChooseOffice(chooseCasual)
                                        setChooseCasual(!chooseCasual)
                                        setChooseParty(chooseCasual)
                                        setChooseCeremony(chooseCasual)
                                        setChooseSport(chooseCasual)
                                        setChooseRelax(chooseCasual)
                                        setChooseStyle("casual")
                                    }}
                                />
                            </Right>
                        </ListItem>
                        <ListItem>
                            <Left>
                                <Text>Party</Text>
                            </Left>
                            <Right>
                                <Radio
                                    disabled={chooseParty}
                                    selectedColor={"#5cb85c"}
                                    selected={chooseParty}
                                    onPress={() => {
                                        setChooseAllStyle(chooseParty)
                                        setChooseOffice(chooseParty)
                                        setChooseCasual(chooseParty)
                                        setChooseParty(!chooseParty)
                                        setChooseCeremony(chooseParty)
                                        setChooseSport(chooseParty)
                                        setChooseRelax(chooseParty)
                                        setChooseStyle("party")
                                    }}
                                />
                            </Right>
                        </ListItem>
                        <ListItem>
                            <Left>
                                <Text>Ceremony</Text>
                            </Left>
                            <Right>
                                <Radio
                                    disabled={chooseCeremony}
                                    selectedColor={"#5cb85c"}
                                    selected={chooseCeremony}
                                    onPress={() => {
                                        setChooseAllStyle(chooseCeremony)
                                        setChooseOffice(chooseCeremony)
                                        setChooseCasual(chooseCeremony)
                                        setChooseParty(chooseCeremony)
                                        setChooseCeremony(!chooseCeremony)
                                        setChooseSport(chooseCeremony)
                                        setChooseRelax(chooseCeremony)
                                        setChooseStyle("ceremony")
                                    }}
                                />
                            </Right>
                        </ListItem>
                        <ListItem>
                            <Left>
                                <Text>Sport</Text>
                            </Left>
                            <Right>
                                <Radio
                                    disabled={chooseSport}
                                    selectedColor={"#5cb85c"}
                                    selected={chooseSport}
                                    onPress={() => {
                                        setChooseAllStyle(chooseSport)
                                        setChooseOffice(chooseSport)
                                        setChooseCasual(chooseSport)
                                        setChooseParty(chooseSport)
                                        setChooseCeremony(chooseSport)
                                        setChooseSport(!chooseSport)
                                        setChooseRelax(chooseSport)
                                        setChooseStyle("sport")
                                    }}
                                />
                            </Right>
                        </ListItem>
                        <ListItem>
                            <Left>
                                <Text>Relax</Text>
                            </Left>
                            <Right>
                                <Radio
                                    disabled={chooseRelax}
                                    selectedColor={"#5cb85c"}
                                    selected={chooseRelax}
                                    onPress={() => {
                                        setChooseAllStyle(chooseRelax)
                                        setChooseOffice(chooseRelax)
                                        setChooseCasual(chooseRelax)
                                        setChooseParty(chooseRelax)
                                        setChooseCeremony(chooseRelax)
                                        setChooseSport(chooseRelax)
                                        setChooseRelax(!chooseRelax)
                                        setChooseStyle("relax")
                                    }}
                                />
                            </Right>
                        </ListItem>
                    </View>
                </Dialog.Content>
                <Dialog.Actions>
                    <Button onPress={() => {
                        hideDialog()
                        onDone()
                    }}>
                        <Text>Done</Text>
                    </Button>
                </Dialog.Actions>
            </Dialog>
            <Dialog
                visible={visibleGender}
                onDismiss={() => hideDialog()}>
                <Dialog.Content>
                    <View>
                        <ListItem>
                            <Left>
                                <Text>All Gender</Text>
                            </Left>
                            <Right>
                                <Radio
                                    disabled={chooseAllGender}
                                    selectedColor={"#5cb85c"}
                                    selected={chooseAllGender}
                                    onPress={() => {
                                        setChooseAllGender(!chooseAllGender)
                                        setChooseWomen(chooseAllGender)
                                        setChooseMen(chooseAllGender)
                                        setChooseGender("%")
                                    }}
                                />
                            </Right>
                        </ListItem>
                        <ListItem>
                            <Left>
                                <Text>Men</Text>
                            </Left>
                            <Right>
                                <Radio
                                    disabled={chooseMen}
                                    selectedColor={"#5cb85c"}
                                    selected={chooseMen}
                                    onPress={() => {
                                        setChooseAllGender(chooseMen)
                                        setChooseWomen(chooseMen)
                                        setChooseMen(!chooseMen)
                                        setChooseGender("men")
                                    }}
                                />
                            </Right>
                        </ListItem>
                        <ListItem>
                            <Left>
                                <Text>Women</Text>
                            </Left>
                            <Right>
                                <Radio
                                    disabled={chooseWomen}
                                    selectedColor={"#5cb85c"}
                                    selected={chooseWomen}
                                    onPress={() => {
                                        setChooseAllGender(chooseWomen)
                                        setChooseWomen(!chooseWomen)
                                        setChooseMen(chooseWomen)
                                        setChooseGender("women")
                                    }}
                                />
                            </Right>
                        </ListItem>
                    </View>
                </Dialog.Content>
                <Dialog.Actions>
                    <Button onPress={() => {
                        hideDialog()
                        onDone()
                    }}>
                        <Text>Done</Text>
                    </Button>
                </Dialog.Actions>
            </Dialog>
        </Container>
    )

}
export default SearchScreen

const styles = StyleSheet.create({
    container: {
        flex: 3,
        backgroundColor: 'white',
        margin: 24,
        marginTop: 16
    },
    background: {
        backgroundColor: 'white',
        margin: 8
    },
    image: {
        flex: 1,
        width: 150,
        height: 180,
        borderRadius: 10,
        margin: 16,
        elevation: 10,
    },
    header: {
        backgroundColor: 'white',
        paddingTop:20
    },
    input: {
        backgroundColor: 'white',
        borderRadius: 10,
        height: 58,
        marginLeft: 16,
        borderColor: 'gray',
        borderWidth: 1
    },
})
