import React, { useState, useEffect } from 'react';
import { Container, Text, Header, Left, Right, Body, Spinner } from 'native-base';
import { StyleSheet, View} from 'react-native';



const users = [
    {
        name: 'Bowtha',
        description: 'Wellcome To my world',
    },
    {
        name: 'Taeyeon',
        description: 'Happy lalala happy happy happy',
    },
];

const Reload = () => {

    const [quote, setQuote] = useState(null)

    const randQuote = () => {
        const thisquote = users[Math.floor(Math.random() * users.length)]
        setQuote(thisquote)
    }

    useEffect(() => {
        randQuote()
    })

    return (
        <Container contentContainerStyle={{ flexGrow: 1 }} >
            <Header style={styles.header}>
                <Left style={{ flex: 1 }} />
                <Body style={{ flex: 2 }} >
                    <Text style={{ fontSize: 24, alignSelf: 'center' }}>STYLISH</Text>
                </Body>
                <Right style={{ flex: 1 }} />
            </Header>
            {
                !quote ?
                    <View style={styles.background}>
                        <Spinner color="black" />
                    </View>
                    :
                    <View style={styles.background} >
                        <View style={{ marginTop: '55%' }}>
                            <Text style={{ fontSize: 24, textAlign: 'center', color: '#EA5E5E' }}>{quote.description}</Text>
                            <Text style={{ fontSize: 16, textAlign: 'center', color: '#EA5E5E' }}>{quote.name}</Text>
                            <Spinner color="black" />
                        </View>
                    </View>
            }
        </Container>
    )
}
export default Reload

const styles = StyleSheet.create({
    background: {
        flex: 1,
        backgroundColor: 'white',
        height: '100%'
    },
    header: {
        backgroundColor: 'white',
        paddingTop:20
    }

})