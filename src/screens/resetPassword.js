import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Text, View, ImageBackground, KeyboardAvoidingView } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { TextInput, Button, Snackbar ,HelperText} from 'react-native-paper'
import { resetPassword, logout } from '../api/index'
import { isLogOut } from '../actions/loginAction';
import { useDispatch, useSelector } from 'react-redux';


const ResetPassword = ({ navigation }) => {

    const token = useSelector(state => state.login.token)
    const dispatch = useDispatch()

    const [password, setPassword] = useState("")
    const [rePassword, setRePassword] = useState("")
    const [isError, setIsError] = useState(false)
    const [canReset, setCanReset] = useState(true)
    const [visible, setVisible] = useState(false)
    const [isMin, setIsmin] = useState(false)

    const checkMin = (text) => {
        if (text.length < 8) {
            setIsmin(true)
        }
        else {
            setIsmin(false)
        }
    }

    const checkPassword = (text) => {
        if (text == rePassword) {
            setCanReset(false)
            setIsError(false)
        }
        else {
            setCanReset(true)
            setIsError(true)
        }
    }

    const checkRePassword = (text) => {
        if (text == password) {
            setCanReset(false)
            setIsError(false)
        }
        else {
            setCanReset(true)
            setIsError(true)
        }
    }

    const onReset = () => {
        const data = {
            "Password": password
        }
        resetPassword(token, data)
            .then(response => {
                setVisible(true)
            })
            .catch(err => console.log(err))
    };

    const onLogout = () => {
        logout(token)
            .then((res) => {
                navigation.navigate("Login")
                setVisible(false)
                dispatch(isLogOut())
            })
            .catch(err => {
                console.log(err)
            })
    }

    const onDismissSnackBar = () => {
        setVisible(false)
    }


    return (
        <ImageBackground
            source={{ uri: 'https://i.pinimg.com/564x/5c/ca/32/5cca3289202ab1491aa87e2e781e6f76.jpg' }}
            style={styles.background}
        >
            <KeyboardAvoidingView behavior="padding">
                <TouchableOpacity onPress={() => navigation.navigate('Login')} style={styles.container} >
                    <FontAwesomeIcon icon={faArrowLeft} />
                </TouchableOpacity>
                <Text style={styles.header}>Reset Password</Text>
                <View style={styles.content}>
                    <TextInput
                        label="Password"
                        value={password}
                        onChangeText={text => {
                            setPassword(text)
                            checkPassword(text)
                            checkMin(text)
                        }}
                        error={isError}
                        error={isMin}
                        style={styles.input}
                        secureTextEntry
                    />
                    <TextInput
                        label="Re-Password"
                        value={rePassword}
                        onChangeText={text => {
                            checkRePassword(text)
                            setRePassword(text)
                            checkMin(text)
                        }}
                        error={isError}
                        error={isMin}
                        style={styles.input}
                        secureTextEntry
                    />
                     <HelperText
                        type="error"
                        visible={isMin}
                    >
                        The password you provided must have at least 8 charectors
                    </HelperText>
                    <Button mode="contained"
                        disabled={canReset}
                        style={styles.button}
                        onPress={() => onReset()}
                    >Reset</Button>
                    <View style={styles.row}>
                        <Text style={styles.text}>go back to profile,</Text>
                        <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
                            <Text style={styles.link}>  click!</Text>
                        </TouchableOpacity>
                    </View>
                </View>

            </KeyboardAvoidingView>
            <Snackbar
                visible={visible}
                onDismiss={onDismissSnackBar}
                action={{
                    label: 'OK',
                    onPress: () => {
                        setVisible(false)
                        onLogout()
                    },
                }}
                style={{ alignSelf: 'flex-end' }}
            >
                Reset Password Success, Please login again.
        </Snackbar>
        </ImageBackground>
    );
};
export default ResetPassword;

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        marginTop: 4,
        alignSelf: 'center'
    },
    link: {
        fontWeight: 'bold',
        textAlign: 'center',
        color: 'white'
    },
    header: {
        fontSize: 26,
        fontWeight: 'bold',
        paddingVertical: 14,
        textAlign: 'center',
        marginTop: 8,
        color: 'gold'
    },
    background: {
        flex: 1,
        width: '100%',
    },
    container: {
        margin: 32
    },
    content: {
        width: '70%',
        marginLeft: '15%',
        marginTop: 32
    },
    input: {
        marginTop: 32,
        height: 48
    },
    button: {
        width: '40%',
        marginLeft: '30%',
        elevation: 15,
        backgroundColor: 'gold',
        marginTop: 50
    },
    text: {
        textAlign: 'center',
        color: 'white'
    }
});

