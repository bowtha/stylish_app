import React from 'react';
import { StyleSheet, Text, ImageBackground, KeyboardAvoidingView, } from 'react-native';
import { Button } from 'react-native-paper'


const LoginScreen = ({ navigation }) => (
    <ImageBackground
        source={{uri: 'https://i.pinimg.com/564x/ad/24/1a/ad241a770c8361ca48fa27839c8f363d.jpg'}}
        style={styles.background}
    >
        <KeyboardAvoidingView style={styles.container} behavior="padding">
            <Text style={styles.header}>STYLISH</Text>
            <Text style={styles.text}>The easiest way to start with STYLISH application.</Text>
            <Button mode="contained" onPress={() => navigation.navigate('LoginForm')} style={styles.login}>Login</Button>
            <Button
                color="black"
                style={styles.signup}
                mode="outlined"
                onPress={() => navigation.navigate('Register')}
            >Sign Up</Button>
        </KeyboardAvoidingView>
    </ImageBackground>
);

export default LoginScreen;

const styles = StyleSheet.create({
    image: {
        width: 128,
        height: 128,
        marginBottom: 12,
    },
    header: {
        fontSize: 26,
        fontWeight: 'bold',
        paddingVertical: 14,
    },
    text: {
        fontSize: 16,
        lineHeight: 26,
        textAlign: 'center',
        marginBottom: 14,
    },
    background: {
        flex: 1,
        width: '100%',
    },
    container: {
        flex: 1,
        padding: 20,
        width: '100%',
        maxWidth: 340,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: '-15%'
    },
    signup: {
        marginTop: 30,
        backgroundColor: 'white',
        elevation:10
    },
    login:{
        backgroundColor:'black',
        elevation:15
    }
});