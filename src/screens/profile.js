import React, { useState, useEffect } from 'react';
import { Container, Text, Header, Left, Right, Button,  Body, Content } from 'native-base';
import { StyleSheet, View, TouchableOpacity, ImageBackground } from 'react-native';
import {  Card, Menu, TextInput, Dialog } from 'react-native-paper';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faSearch, faHome } from '@fortawesome/free-solid-svg-icons'
import { useSelector, useDispatch } from 'react-redux';
import { updateUsername, getUserData, logout } from '../api/index'
import { isLogOut } from '../actions/loginAction'


const ProfileScreen = ({ navigation }) => {

    const dispatch = useDispatch()
    const token = useSelector(state => state.login.token)

    const [visible, setVisible] = useState(false)
    const [isError, setIsError] = useState(false)
    const [newName, setNewName] = useState("")

    const [userData, setUserData] = useState({})

    const hideDialog = () => {
        setVisible(false)
    }
    const setLogout = () => {
        logout(token)
            .then(() => {
                dispatch(isLogOut())
                navigation.navigate("Home")
            })
            .catch(err => console.log(err))
    }

    const update = () => {
        if (newName === "") {
            setIsError(true)
        }
        else {
            const data = {
                "Username": newName
            }
            updateUsername(token, data)
                .then(response => {
                    setVisible(false)
                    fetchUserData()
                })
                .catch(err => console.log(err))
        }
    }

    const fetchUserData = () => {
        getUserData(token)
            .then(response => {
                setUserData(response.data)
                console.log(response.data)
            })
            .catch(err => console.log(err))
    }

    useEffect(() => {
        if (token != null) {
            fetchUserData()
        }
    }, [])

    return (
        <Container contentContainerStyle={{ flexGrow: 1 }} >
            <ImageBackground source={{ uri: 'https://i.pinimg.com/474x/bf/60/0c/bf600c45ee57f874c2f900f6066a237b.jpg' }} style={styles.background}>
                <Header style={styles.header}>
                    <Left style={{ flex: 1 }}>
                        <TouchableOpacity style={{ marginLeft: 8 }} onPress={() => navigation.navigate('Home')}>
                            <FontAwesomeIcon icon={faHome} size={24} />
                        </TouchableOpacity>
                    </Left>
                    <Body style={{ flex: 2 }} >
                        <Text style={{ fontSize: 24, alignSelf: 'center' }}>STYLISH</Text>
                    </Body>
                    <Right style={{ flex: 1 }}>
                        <TouchableOpacity style={{ marginRight: 8 }} onPress={() => navigation.navigate('Search')}>
                            <FontAwesomeIcon icon={faSearch} size={24} />
                        </TouchableOpacity>
                    </Right>
                </Header>
                {
                    !userData ?
                        null
                        :
                        <Content>
                            <View style={{ flex: 1 }}>
                                <View style={{ marginTop: '30%', backgroundColor: 'white', width: '50%', marginLeft: '25%', borderRadius: 10, padding: 10 }}>
                                    <Text style={{ textAlign: 'center', fontSize: 24 }}>{userData.Username}</Text>
                                </View>
                            </View>
                            <Card style={{ flex: 1, width: '80%', marginLeft: '10%', marginTop: '20%', elevation: 10 }}>
                                <Menu.Item icon="format-text" onPress={() => { setVisible(true) }} title="Change Username" />
                                <Menu.Item icon="eye-off" onPress={() => navigation.navigate("ResetPassword")} title="Reset Password" />
                                <Menu.Item icon="plus-box" onPress={() => navigation.navigate('AddCollection')} title="Add Collection" />
                                <Menu.Item icon="table-of-contents" onPress={() => { navigation.navigate('MyCollection') }} title="My Collection" />
                            </Card>
                            <Button
                                style={styles.logout}
                                onPress={() => setLogout()}
                            >
                                <Text >Logout</Text>
                            </Button>
                        </Content>
                }

                <Dialog
                    visible={visible}
                    onDismiss={hideDialog}>
                    <Dialog.Title>Change Username</Dialog.Title>
                    <Dialog.Content>
                        <TextInput
                            error={isError}
                            placeholder="username"
                            style={styles.input}
                            onChangeText={text => {
                                setNewName(text)
                                setIsError(false)
                            }} />
                    </Dialog.Content>
                    <Dialog.Actions>
                        <Button onPress={() => update()}><Text>Done</Text></Button>
                    </Dialog.Actions>
                </Dialog>
            </ImageBackground>
        </Container>
    )
}
export default ProfileScreen

const styles = StyleSheet.create({
    background: {
        flex: 1,
    },
    header: {
        backgroundColor: 'white',
        paddingTop:20
    },
    input: {
        height: 50
    },
    logout: {
        width: '30%',
        marginLeft: '35%',
        marginTop: 30,
        backgroundColor: 'black',
        justifyContent: 'center',
        elevation: 15,
        marginBottom: 30
    }
})
