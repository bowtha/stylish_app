import React, { useState, useEffect } from 'react';
import { Container, Text,  Content, Spinner, } from 'native-base';
import { StyleSheet, View, FlatList, ImageBackground, Platform } from 'react-native';
import { Card, Button, Dialog, TextInput } from 'react-native-paper';
import ImagePicker from 'react-native-image-picker'
import { postItem,  getCollectionByID } from '../api';
import { useSelector } from 'react-redux';

const AddItems = ({ route, navigation }) => {
    const collection_id = route.params.collection_id
    const token = useSelector(state => state.login.token)

    const [photo, setPhoto] = useState([])
    const [isLoading, setIsLoading] = useState(false)
    const [name, setName] = useState("")
    const [visibleAddImage, setVisibleAddImage] = useState(false)
    const [image, setImage] = useState(null)

    const Image = () => {

        const options = {
            title: 'Select Photo',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.showImagePicker(options, (image) => {
            if (image.didCancel) {
                console.log('User cancelled image picker');
            } else if (image.error) {
                console.log('ImagePicker Error: ', image.error);
            } else {
                const uri = image.uri
                const type = image.type;
                const name = image.fileName;
                const source = {
                    type,
                    name,
                    uri:
                        Platform.OS === "android" ? uri : uri.replace("file://", "")
                }
                cloudinaryUpload(source)
                setVisibleAddImage(false)
            }
        });
    }

    const cloudinaryUpload = (photo) => {
        const data = new FormData()
        data.append('file', photo)
        data.append('upload_preset', 'stylish')
        data.append("cloud_name", "stylish")
        setIsLoading(true)
        fetch("https://api.cloudinary.com/v1_1/stylish/image/upload", {
            method: "post",
            body: data
        }).then(res => res.json()).
            then(data => {
                setImage(data.url)
                setIsLoading(false)
            }).catch(err => {
                alert("An Error Occured While Uploading")
                setIsLoading(false)
            })
    }

    const hideDialog = () => {
        setVisibleAddImage(false)
    }

    const uploadTosever = () => {
        const addGo = {
            "Name": name,
            "Image": image
        }
        postItem(token, collection_id, addGo)
            .then(res => {
                getCollectionByID(token, collection_id)
                    .then(response => {
                        setPhoto(response.data)
                        setIsLoading(false)
                        setImage(null)
                    })
                    .catch(err => {
                        console.log(err)
                        setIsLoading(false)
                    })
                console.log(res)
            })
            .catch(err => {
                console.log(err)
                setIsLoading(false)
            })
    }

    useEffect(() => {

    }, [])

    return (
        <Container contentContainerStyle={{ flexGrow: 1 }} >
            <ImageBackground source={{ uri: 'https://i.pinimg.com/474x/bf/60/0c/bf600c45ee57f874c2f900f6066a237b.jpg' }} style={styles.background}>
                <Content >
                    <View>
                        <Text style={styles.header}>ADD ITEMS</Text>
                    </View>
                    {
                        !photo ?
                            <Button
                                style={{ marginBottom: '10%', flex: 1, width: '30%', marginLeft: '35%', backgroundColor: 'white', marginTop: '10%', elevation: 10 }}
                                mode="contained"
                                onPress={() => Image()}
                            ><Text style-={{ fontSize: 32 }}>ADD</Text></Button>
                            :
                            photo.length == 6 ?
                                null
                                :
                                <Button
                                    style={{ marginBottom: '10%', flex: 1, width: '30%', marginLeft: '35%', backgroundColor: 'white', marginTop: '10%', elevation: 10 }}
                                    mode="contained"
                                    onPress={() => setVisibleAddImage(true)}
                                ><Text style-={{ fontSize: 32 }}>ADD</Text></Button>
                    }
                    {
                        !image ?
                            <Card style={styles.card}>
                                {
                                    !photo ?
                                        null
                                        :
                                        isLoading == true ?
                                            <Spinner color='green' />
                                            :
                                            <FlatList
                                                data={photo}
                                                renderItem={img => (
                                                    <Card style={styles.image} >
                                                        <Card.Cover source={{ uri: `${img.item.Image}` }} />
                                                        <Card.Content><Text style={{textAlign:'center',marginTop:3}}>{img.item.Name}</Text></Card.Content>
                                                    </Card>
                                                )}
                                                numColumns={2}
                                                keyExtractor={(img, index) => index}
                                            />
                                }
                            </Card>
                            :
                            <Card style={styles.preview} >
                                <Card.Cover source={{ uri: `${image}` }} />
                                <Card.Content><Text style={{textAlign:'center',marginTop:3}}>{name}</Text></Card.Content>
                                <Card.Actions>
                                    <View style={{ justifyContent: 'flex-start', display: 'flex', width: '50%' }}>
                                        <Button
                                            style={{ backgroundColor: "gray", width: '100%' }}
                                            color={"black"}
                                            onPress={() => {
                                                setImage(null)
                                            }}
                                        >Undo</Button>
                                    </View>
                                    <View style={{ alignItems: 'flex-end', display: 'flex', width: '50%' }}>
                                        <Button
                                            style={{ backgroundColor: 'gold', width: '90%' }}
                                            color={"black"}
                                            onPress={() => uploadTosever()}
                                        >OK</Button>
                                    </View>
                                </Card.Actions>
                            </Card>
                    }
                    {
                        !photo ?
                            null
                            :
                            image != null ?
                                null
                                :
                                photo.length >= 4 ?
                                    <Button
                                        style={{ marginBottom: '10%', flex: 1, width: '50%', marginLeft: '25%', backgroundColor: 'gold', marginTop: '10%' }}
                                        mode="contained"
                                        // disabled={canSubmit}
                                        onPress={() => navigation.navigate("ViewCollection", {
                                            collection_id: collection_id
                                        })}
                                    ><Text style-={{ fontSize: 32 }}>ADD COLLECTION</Text></Button>
                                    :
                                    null
                    }
                </Content>
                <Dialog
                    visible={visibleAddImage}
                    onDismiss={hideDialog}>
                    <Dialog.Title>ADD ITEMS</Dialog.Title>
                    <Dialog.Content>
                        <TextInput placeholder="name" style={styles.input} onChangeText={text => {
                            setName(text)
                        }}>
                        </TextInput>
                        {
                            name === "" ?
                                null
                                :
                                <Button onPress={() => Image()} mode="contained" style={styles.button}>Choose Image</Button>
                        }

                    </Dialog.Content>
                    <Dialog.Actions>
                        <Button onPress={() => {
                            hideDialog()
                        }}>Upload</Button>
                    </Dialog.Actions>
                </Dialog>
            </ImageBackground>
        </Container>
    )
}
export default AddItems;

const styles = StyleSheet.create({
    background: {
        flex: 1,
        paddingBottom: 20
    },
    container: {
        backgroundColor: 'white',
        margin: 24,
        marginTop: 16,
        height: '75%'
    },
    header: {
        fontSize: 26,
        fontWeight: 'bold',
        paddingVertical: 14,
        color: 'gold',
        textAlign: 'center',
        marginTop: 30
    },
    image: {
        flex: 1,
        margin: '5%',
        backgroundColor: 'white',
        width: '100%',
        height: '90%',
        elevation: 10,
    },
    card: {
        backgroundColor: 'white',
        width: '90%',
        flex: 1,
        marginLeft: '5%',
        marginTop: '5%',
    },
    input: {
        fontSize: 12,
        width: '100%',
        margin: '2%',
        height: 40,
        elevation: 10
    },
    column: {
        width: '45%',
        margin: '2%'
    },
    button: {
        backgroundColor: 'gold',
        width: '70%',
        marginLeft: '15%',
        marginTop: 30,
        elevation: 20
    },
    preview: {
        width: '60%',
        alignSelf: 'center',
    }
})
