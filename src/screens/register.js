import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Text, View, ImageBackground, KeyboardAvoidingView } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { TextInput, Button, HelperText } from 'react-native-paper'
import { signup } from '../api';
import { isLogin } from '../actions/loginAction';
import { useDispatch } from 'react-redux';


const RegisterScreen = ({ navigation }) => {

    const dispatch = useDispatch()

    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");


    const [isError, setIsError] = useState(false)
    const [match, setMatch] = useState(false)
    const [onsignUp, setOnsingup] = useState(false)
    const [isMin, setIsmin] = useState(false)
    const [isUsername, setIsUsername] = useState(false)

    const checkEmail = (text) => {
        const reg = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,15}/;
        if (reg.test(text) === true) {
            setMatch(false)
            setOnsingup(false)
        }
        else {
            setMatch(true)
            setOnsingup(true)
        }
    }

    const checkMin = (text) => {
        if (text.length < 8) {
            setIsmin(true)
        }
        else {
            setIsmin(false)
        }
    }

    const checkUsername = (text) => {
        if (text.length > 0) {
            setIsUsername(false)
        }
        else {
            setIsUsername(true)
        }
    }

    const onSignUp = () => {
        if (name == "" || email == "" || password == "") {
            setOnsingup(true)
        }
        else {
            const data = {
                "Username": name,
                "Email": email,
                "Password": password,
            }
            signup(data)
                .then((response) => {
                    dispatch(isLogin(response.data.token))
                    navigation.navigate("Home", {
                        user_id: response.data.user_id
                    })
                })
                .catch(err => {
                    console.log(err)
                    setIsError(true)
                })
        }

    };

    return (
        <ImageBackground
            source={{uri :'https://i.pinimg.com/564x/b7/64/e1/b764e14b38062b6636c991e88e93e7c1.jpg'}}
            style={styles.background}
        >
            <KeyboardAvoidingView behavior="padding">

                <TouchableOpacity onPress={() => navigation.navigate('Login')} style={styles.container} >
                    <FontAwesomeIcon icon={faArrowLeft} />
                </TouchableOpacity>
                <Text style={styles.header}>Create Account</Text>
                <View style={styles.content}>
                    <TextInput
                        label="Name"
                        returnKeyType="next"
                        value={name}
                        onChangeText={text => {
                            setName(text)
                            checkUsername(text)
                        }}
                        error={isError}
                        error={isUsername}
                        style={styles.input}
                    />
                    {
                        isUsername == false ?
                            null
                            :
                            <HelperText
                                type="error"
                                visible={isUsername}
                            >
                                Please enter your username
                    </HelperText>
                    }


                    <TextInput
                        label="Email"
                        returnKeyType="next"
                        value={email}
                        onChangeText={text => {
                            setEmail(text)
                            checkEmail(text)
                        }}
                        error={isError}
                        error={match}
                        autoCapitalize="none"
                        autoCompleteType="email"
                        textContentType="emailAddress"
                        keyboardType="email-address"
                        style={styles.input}
                    />
                    {
                        match == true ?
                            <HelperText
                                type="error"
                                visible={match}
                            >
                                Please type your e-mail address in format yourname@example.com
                    </HelperText>
                            :
                            null
                    }


                    <TextInput
                        label="Password"
                        returnKeyType="done"
                        value={password}
                        onChangeText={text => {
                            setPassword(text)
                            checkMin(text)
                        }}
                        error={isError}
                        error={isMin}
                        secureTextEntry
                        style={styles.input}
                    />
                    <HelperText
                        type="error"
                        visible={isMin}
                    >
                        The password you provided must have at least 8 charectors
                    </HelperText>

                    <Button mode="contained"
                        disabled={onsignUp}
                        onPress={() => onSignUp()}
                        style={styles.button}> Sign Up</Button>

                    <View style={styles.row}>
                        <Text style={styles.label}>Already have an account? </Text>
                        <TouchableOpacity onPress={() => navigation.navigate('LoginScreen')}>
                            <Text style={styles.link}>Login</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </KeyboardAvoidingView>
        </ImageBackground>
    );
};
export default RegisterScreen;

const styles = StyleSheet.create({
    label: {

    },
    button: {
        marginTop: 24,
        width: '50%',
        backgroundColor: 'gold',
        elevation: 15,
        marginLeft: '25%'
    },
    row: {
        flexDirection: 'row',
        marginTop: 4,
        alignSelf: 'center',
    },
    link: {
        fontWeight: 'bold',
        color: 'gold'
    },
    background: {
        flex: 1,
        width: '100%',
    },
    container: {
        margin: 32
    },
    header: {
        fontSize: 26,
        fontWeight: 'bold',
        paddingVertical: 14,
        textAlign: 'center',
        marginTop: 16,
        color: 'gold',
    },
    content: {
        width: '80%',
        marginLeft: '10%',
        marginTop: 32
    },
    input: {
        marginTop: 8,
        height: 56
    },
});

