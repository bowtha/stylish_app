import React, { useState, useEffect } from 'react';
import { Container, Text, Header, Left, Right, Body, Content } from 'native-base';
import { StyleSheet, TouchableOpacity, ImageBackground } from 'react-native';
import { Card } from 'react-native-paper';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faUserCircle, faHome, faFolder, faFolderOpen } from '@fortawesome/free-solid-svg-icons'
import { useSelector } from 'react-redux';
import { getCollectionByUserID } from '../api';


const MyCollectionScreen = ({ navigation }) => {


    const token = useSelector(state => state.login.token)

    const [list, setList] = useState([])

    const fetchCollection = () => {
        getCollectionByUserID(token)
            .then(response => {
                setList(response.data)
            })
            .catch(err => console.log(err))
    }

    useEffect(() => {
        fetchCollection()
    }, [])

    return (
        <Container contentContainerStyle={{ flexGrow: 1 }} >
            <ImageBackground source={{ uri: 'https://i.pinimg.com/564x/c6/2b/23/c62b2311eb56e1eb41af97c6056953bd.jpg' }} style={styles.background}>
                <Header style={styles.header}>
                    <Left style={{ flex: 1 }}>
                        <TouchableOpacity style={{ marginLeft: 8 }} onPress={() => navigation.navigate('Profile')}>
                            <FontAwesomeIcon icon={faUserCircle} size={24} />
                        </TouchableOpacity>
                    </Left>
                    <Body style={{ flex: 2 }} >
                        <Text style={{ fontSize: 24, alignSelf: 'center' }}>STYLISH</Text>
                    </Body>
                    <Right style={{ flex: 1 }}>
                        <TouchableOpacity style={{ marginRight: 8 }} onPress={() => navigation.navigate('Home')}>
                            <FontAwesomeIcon icon={faHome} size={24} />
                        </TouchableOpacity>
                    </Right>
                </Header>
                <Content>

                    {
                        list.length == 0 ?
                            <Card style={styles.nodata} >
                                <Card.Content>
                                    <FontAwesomeIcon icon={faFolderOpen} color="gray" style={{ alignSelf: 'center', margin: 10 }} size={24} />
                                    <Text>you don't have collection</Text>
                                </Card.Content>
                            </Card>
                            :
                            list.map(item => {
                                return (
                                    <Card
                                        style={styles.card}
                                        onPress={() => navigation.navigate("ViewCollection", {
                                            collection_id: item.ID
                                        })}>
                                        <Card.Content style={{ flexDirection: 'row' }}>
                                            <FontAwesomeIcon icon={faFolder} style={styles.icon} color='gold' />
                                            <Text style={styles.text}>{item.Name}</Text>
                                        </Card.Content>
                                    </Card>
                                )
                            })
                    }
                </Content>
            </ImageBackground>
        </Container >
    )
}
export default MyCollectionScreen

const styles = StyleSheet.create({
    background: {
        flex: 1,
        paddingBottom: 20
    },
    header: {
        backgroundColor: 'white',
        paddingTop:20
    },
    card: {
        width: '60%',
        marginLeft: '20%',
        marginTop: 20,
        elevation: 10
    },
    text: {
        fontSize: 18,
        marginLeft: 30
    },
    icon: {
        alignSelf: 'center',
        marginLeft: 16
    },
    nodata: {
        width: '60%',
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '50%',
        opacity: 0.7
    }
})
