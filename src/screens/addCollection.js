import React, { useState, useEffect } from 'react';
import { Container, Text, Header, Left, Right, Body, Content, Item, Label, Input, Radio, ListItem, } from 'native-base';
import { StyleSheet, View, ScrollView, FlatList, TouchableOpacity, ImageBackground } from 'react-native';
import { Card, Button, Dialog } from 'react-native-paper';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faSearch, faUserCircle, faHome } from '@fortawesome/free-solid-svg-icons'
import { postCollection } from '../api';
import { useSelector } from 'react-redux';


const AddCollection = ({ navigation }) => {

    const token = useSelector(state => state.login.token)

    const [expanded, setExpanded] = useState(false)
    const [visibleLocation, setVisibleLocation] = useState(false)
    const [visibleStyle, setVisibleStyle] = useState(false)
    const [collecttionName, setCollectionName] = useState("")
    const [isErrorName, setIsErrorName] = useState(false)

    const [women, setWomen] = useState(false)
    const [men, setMen] = useState(false)
    const [gender, setGender] = useState("")

    const [chooseLocation, setChooseLocation] = useState("")
    const [chooseTH, setChooseTH] = useState(false)
    const [chooseNYC, setChooseNYC] = useState(false)
    const [choosePS, setChoosePS] = useState(false)
    const [chooseLD, setChooseLD] = useState(false)
    const [chooseTK, setChooseTK] = useState(false)
    const [chooseKR, setChooseKR] = useState(false)

    const [chooseStyle, setChooseStyle] = useState("")
    const [chooseOffice, setChooseOffice] = useState(false)
    const [chooseCasual, setChooseCasual] = useState(false)
    const [chooseParty, setChooseParty] = useState(false)
    const [chooseCeremony, setChooseCeremony] = useState(false)
    const [chooseSport, setChooseSport] = useState(false)
    const [chooseRelax, setChooseRelax] = useState(false)

    const [chooseSeason, setChooseSeason] = useState("")
    const [chooseSpring, setChooseSpring] = useState(false)
    const [chooseSummer, setChooseSummer] = useState(false)
    const [chooseWinter, setChooseWinter] = useState(false)
    const [chooseAutumn, setChooseAutumn] = useState(false)
    const [chooseRainy, setChooseRainy] = useState(false)

    const showDialogLocation = () => {
        setVisibleLocation(true)
    }
    const showDialogStyle = () => {
        setVisibleStyle(true)
    }
    const hideDialog = () => {
        setVisibleLocation(false)
        setVisibleStyle(false)
        setExpanded(false)
    }

    const addCollection = () => {
        if (collecttionName == "") {
            setIsErrorName(true)
        }
        if (chooseLocation == "") {
            setChooseLocation("TH")
        }
        if (chooseStyle == "") {
            setChooseStyle("casual")
        }
        if (gender == ""){
            setGender("women")
            setWomen(true)
        }
        if (chooseSeason == ""){
            setGender("rainy")
            setChooseRainy(true)
        }
        else {
            const data = {
                "Name": collecttionName,
                "Location": chooseLocation,
                "Style": chooseStyle,
                "Seasons": chooseSeason,
                "Gender": gender
            }
            console.log('data--->',data)
            postCollection(token, data)
                .then(response => {
                    navigation.navigate("AddItems", {
                        collection_id: response.data.collection_id
                    })
                    setIsErrorName(false)
                })
                .catch(err => console.log(err))
        }
    }

    useEffect(() => {

    }, [])

    return (
        <Container contentContainerStyle={{ flexGrow: 1 }} >
            <ImageBackground source={{ uri: 'https://i.pinimg.com/474x/bf/60/0c/bf600c45ee57f874c2f900f6066a237b.jpg' }} style={styles.background}>
                <Header style={styles.header}>
                    <Left style={{ flex: 1 }}>
                        <TouchableOpacity style={{ marginLeft: 8 }} onPress={() => navigation.navigate('Profile')}>
                            <FontAwesomeIcon icon={faUserCircle} size={24} />
                        </TouchableOpacity>
                    </Left>
                    <Body style={{ flex: 2 }} >
                        <Text style={{ fontSize: 24, alignSelf: 'center' }}>STYLISH</Text>
                    </Body>
                    <Right style={{ flex: 1 }}>
                        <TouchableOpacity style={{ marginRight: 16 }} onPress={() => navigation.navigate('Home')}>
                            <FontAwesomeIcon icon={faHome} size={24} />
                        </TouchableOpacity>
                        <TouchableOpacity style={{ marginRight: 8 }} onPress={() => navigation.navigate('Search')}>
                            <FontAwesomeIcon icon={faSearch} size={24} />
                        </TouchableOpacity>
                    </Right>
                </Header>
                <Content >
                    <Card style={{ flex: 1, width: '90%', margin: '5%', padding: '10%', elevation: 5 }}>
                        <Item floatingLabel error={isErrorName}>
                            <Label>Collecttion name</Label>
                            <Input
                                value={collecttionName}
                                onChangeText={(t) => {
                                    setCollectionName(t)
                                    setIsErrorName(false)
                                }}
                            />
                        </Item>
                        <Item style={{ marginTop: 15, flexDirection: 'column' }}>
                            <Label style={{ textAlign: 'left', alignSelf: 'flex-start', color: 'gray' }}>
                                <Text style={{ color: 'gray' }}>Gender</Text>
                            </Label>
                            <View style={{ flexDirection: 'row' }}>
                                <Button
                                    disabled={men}
                                    color={'gold'}
                                    onPress={() => {
                                        setGender("men")
                                        setMen(!men)
                                        setWomen(men)
                                    }}>Men</Button>
                                <Button
                                    disabled={women}
                                    color={'gold'}
                                    onPress={() => {
                                        setGender("women")
                                        setMen(women)
                                        setWomen(!women)
                                    }}>Women</Button>
                            </View>
                        </Item>
                        <Item>
                            <View style={{ flexDirection: 'row', marginTop: 15, width: '100%' }}>
                                <Button style={{ width: '50%' }} color={'gold'} onPress={() => showDialogLocation()}>Location</Button>
                                <Text style={{ fontSize: 15, marginTop: 7, textAlign: 'center', width: '50%' }}>{chooseLocation}</Text>
                            </View>
                        </Item>
                        <Item>
                            <View style={{ flexDirection: 'row', marginTop: 15 }}>
                                <Button style={{ width: '50%' }} color={'gold'} onPress={() => showDialogStyle()}>Style</Button>
                                <Text style={{ fontSize: 15, marginTop: 7, textAlign: 'center', width: '50%' }}>{chooseStyle}</Text>
                            </View>
                        </Item>
                        <Item style={{ marginTop: 15, flexDirection: 'column' }}>
                            <Label style={{ textAlign: 'left', alignSelf: 'flex-start', color: 'gray' }}>
                                <Text style={{ color: 'gray' }}>Seasons</Text>
                            </Label>
                            <View >
                                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                    <Button
                                        disabled={chooseWinter}
                                        color={'gold'}
                                        onPress={() => {
                                            setChooseSeason("winter")
                                            setChooseWinter(!chooseWinter)
                                            setChooseAutumn(chooseWinter)
                                            setChooseSpring(chooseWinter)
                                            setChooseSummer(chooseWinter)
                                            setChooseRainy(chooseWinter)
                                        }} >Winter</Button>
                                    <Button
                                        disabled={chooseSpring}
                                        color={'gold'}
                                        onPress={() => {
                                            setChooseSeason("spring")
                                            setChooseWinter(chooseSpring)
                                            setChooseAutumn(chooseSpring)
                                            setChooseSpring(!chooseSpring)
                                            setChooseSummer(chooseSpring)
                                            setChooseRainy(chooseSpring)
                                        }}>Spring</Button>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <Button
                                        disabled={chooseSummer}
                                        color={'gold'}
                                        onPress={() => {
                                            setChooseSeason("summer")
                                            setChooseWinter(chooseSummer)
                                            setChooseAutumn(chooseSummer)
                                            setChooseSpring(chooseSummer)
                                            setChooseSummer(!chooseSummer)
                                            setChooseRainy(chooseSummer)
                                        }}>Summer</Button>
                                    <Button
                                        disabled={chooseAutumn}
                                        color={'gold'}
                                        onPress={() => {
                                            setChooseSeason("autumn")
                                            setChooseWinter(chooseAutumn)
                                            setChooseAutumn(!chooseAutumn)
                                            setChooseSpring(chooseAutumn)
                                            setChooseSummer(chooseAutumn)
                                            setChooseRainy(chooseAutumn)
                                        }}>Autumn</Button>
                                    <Button
                                        disabled={chooseRainy}
                                        color={'gold'}
                                        onPress={() => {
                                            setChooseSeason("rainy")
                                            setChooseWinter(chooseRainy)
                                            setChooseAutumn(chooseRainy)
                                            setChooseSpring(chooseRainy)
                                            setChooseSummer(chooseRainy)
                                            setChooseRainy(!chooseRainy)
                                        }}>Rainy</Button>
                                </View>
                            </View>
                        </Item>
                    </Card>
                    <Button
                        onPress={() => addCollection()}
                        mode="contained"
                        style={{ flex: 1, width: '30%', marginLeft: '35%', backgroundColor: 'black', elevation: 20 }}
                    >NEXT</Button>
                </Content>
                <Dialog
                    visible={visibleLocation}
                    onDismiss={() => hideDialog()}>
                    <Dialog.Content>
                        <View>
                            <ListItem>
                                <Left>
                                    <Text>Thailand</Text>
                                </Left>
                                <Right>
                                    <Radio
                                        disabled={chooseTH}
                                        selectedColor={"#5cb85c"}
                                        selected={chooseTH}
                                        onPress={() => {
                                            setChooseTH(!chooseTH)
                                            setChooseNYC(chooseTH)
                                            setChoosePS(chooseTH)
                                            setChooseLD(chooseTH)
                                            setChooseTK(chooseTH)
                                            setChooseKR(chooseTH)
                                            setChooseLocation("TH")
                                        }}
                                    />
                                </Right>
                            </ListItem>
                            <ListItem>
                                <Left>
                                    <Text>NYC</Text>
                                </Left>
                                <Right>
                                    <Radio
                                        disabled={chooseNYC}
                                        selectedColor={"#5cb85c"}
                                        selected={chooseNYC}
                                        onPress={() => {
                                            setChooseTH(chooseNYC)
                                            setChooseNYC(!chooseNYC)
                                            setChoosePS(chooseNYC)
                                            setChooseLD(chooseNYC)
                                            setChooseTK(chooseNYC)
                                            setChooseKR(chooseNYC)
                                            setChooseLocation("NYC")
                                        }}
                                    />
                                </Right>
                            </ListItem>
                            <ListItem>
                                <Left>
                                    <Text>Paris</Text>
                                </Left>
                                <Right>
                                    <Radio
                                        disabled={choosePS}
                                        selectedColor={"#5cb85c"}
                                        selected={choosePS}
                                        onPress={() => {
                                            setChooseTH(choosePS)
                                            setChooseNYC(choosePS)
                                            setChoosePS(!choosePS)
                                            setChooseLD(choosePS)
                                            setChooseTK(choosePS)
                                            setChooseKR(choosePS)
                                            setChooseLocation("PS")
                                        }}
                                    />
                                </Right>
                            </ListItem>
                            <ListItem>
                                <Left>
                                    <Text>London</Text>
                                </Left>
                                <Right>
                                    <Radio
                                        disabled={chooseLD}
                                        selectedColor={"#5cb85c"}
                                        selected={chooseLD}
                                        onPress={() => {
                                            setChooseTH(chooseLD)
                                            setChooseNYC(chooseLD)
                                            setChoosePS(chooseLD)
                                            setChooseLD(!chooseLD)
                                            setChooseTK(chooseLD)
                                            setChooseKR(chooseLD)
                                            setChooseLocation("LD")
                                        }}
                                    />
                                </Right>
                            </ListItem>
                            <ListItem>
                                <Left>
                                    <Text>Tokyo</Text>
                                </Left>
                                <Right>
                                    <Radio
                                        disabled={chooseTK}
                                        selectedColor={"#5cb85c"}
                                        selected={chooseTK}
                                        onPress={() => {
                                            setChooseTH(chooseTK)
                                            setChooseNYC(chooseTK)
                                            setChoosePS(chooseTK)
                                            setChooseLD(chooseTK)
                                            setChooseTK(!chooseTK)
                                            setChooseKR(chooseTK)
                                            setChooseLocation("TK")
                                        }}
                                    />
                                </Right>
                            </ListItem>
                            <ListItem>
                                <Left>
                                    <Text>Korea</Text>
                                </Left>
                                <Right>
                                    <Radio
                                        disabled={chooseKR}
                                        selectedColor={"#5cb85c"}
                                        selected={chooseKR}
                                        onPress={() => {
                                            setChooseTH(chooseKR)
                                            setChooseNYC(chooseKR)
                                            setChoosePS(chooseKR)
                                            setChooseLD(chooseKR)
                                            setChooseTK(chooseKR)
                                            setChooseKR(!chooseKR)
                                            setChooseLocation("KR")
                                        }}
                                    />
                                </Right>
                            </ListItem>
                        </View>
                    </Dialog.Content>
                    <Dialog.Actions>
                        <Button onPress={() => {
                            hideDialog()
                        }}>
                            <Text>Done</Text>
                        </Button>
                    </Dialog.Actions>
                </Dialog>
                <Dialog
                    visible={visibleStyle}
                    onDismiss={() => hideDialog()}>
                    <Dialog.Content>
                        <View>
                            <ListItem>
                                <Left>
                                    <Text>Office</Text>
                                </Left>
                                <Right>
                                    <Radio
                                        disabled={chooseOffice}
                                        selectedColor={"#5cb85c"}
                                        selected={chooseOffice}
                                        onPress={() => {
                                            setChooseOffice(!chooseOffice)
                                            setChooseCasual(chooseOffice)
                                            setChooseParty(chooseOffice)
                                            setChooseCeremony(chooseOffice)
                                            setChooseSport(chooseOffice)
                                            setChooseRelax(chooseOffice)
                                            setChooseStyle("office")
                                        }}
                                    />
                                </Right>
                            </ListItem>
                            <ListItem>
                                <Left>
                                    <Text>Casual</Text>
                                </Left>
                                <Right>
                                    <Radio
                                        disabled={chooseCasual}
                                        selectedColor={"#5cb85c"}
                                        selected={chooseCasual}
                                        onPress={() => {
                                            setChooseOffice(chooseCasual)
                                            setChooseCasual(!chooseCasual)
                                            setChooseParty(chooseCasual)
                                            setChooseCeremony(chooseCasual)
                                            setChooseSport(chooseCasual)
                                            setChooseRelax(chooseCasual)
                                            setChooseStyle("casual")
                                        }}
                                    />
                                </Right>
                            </ListItem>
                            <ListItem>
                                <Left>
                                    <Text>Party</Text>
                                </Left>
                                <Right>
                                    <Radio
                                        disabled={chooseParty}
                                        selectedColor={"#5cb85c"}
                                        selected={chooseParty}
                                        onPress={() => {
                                            setChooseOffice(chooseParty)
                                            setChooseCasual(chooseParty)
                                            setChooseParty(!chooseParty)
                                            setChooseCeremony(chooseParty)
                                            setChooseSport(chooseParty)
                                            setChooseRelax(chooseParty)
                                            setChooseStyle("party")
                                        }}
                                    />
                                </Right>
                            </ListItem>
                            <ListItem>
                                <Left>
                                    <Text>Ceremony</Text>
                                </Left>
                                <Right>
                                    <Radio
                                        disabled={chooseCeremony}
                                        selectedColor={"#5cb85c"}
                                        selected={chooseCeremony}
                                        onPress={() => {
                                            setChooseOffice(chooseCeremony)
                                            setChooseCasual(chooseCeremony)
                                            setChooseParty(chooseCeremony)
                                            setChooseCeremony(!chooseCeremony)
                                            setChooseSport(chooseCeremony)
                                            setChooseRelax(chooseCeremony)
                                            setChooseStyle("ceremony")
                                        }}
                                    />
                                </Right>
                            </ListItem>
                            <ListItem>
                                <Left>
                                    <Text>Sport</Text>
                                </Left>
                                <Right>
                                    <Radio
                                        disabled={chooseSport}
                                        selectedColor={"#5cb85c"}
                                        selected={chooseSport}
                                        onPress={() => {
                                            setChooseOffice(chooseSport)
                                            setChooseCasual(chooseSport)
                                            setChooseParty(chooseSport)
                                            setChooseCeremony(chooseSport)
                                            setChooseSport(!chooseSport)
                                            setChooseRelax(chooseSport)
                                            setChooseStyle("sport")
                                        }}
                                    />
                                </Right>
                            </ListItem>
                            <ListItem>
                                <Left>
                                    <Text>Relax</Text>
                                </Left>
                                <Right>
                                    <Radio
                                        disabled={chooseRelax}
                                        selectedColor={"#5cb85c"}
                                        selected={chooseRelax}
                                        onPress={() => {
                                            setChooseOffice(chooseRelax)
                                            setChooseCasual(chooseRelax)
                                            setChooseParty(chooseRelax)
                                            setChooseCeremony(chooseRelax)
                                            setChooseSport(chooseRelax)
                                            setChooseRelax(!chooseRelax)
                                            setChooseStyle("relax")
                                        }}
                                    />
                                </Right>
                            </ListItem>
                        </View>
                    </Dialog.Content>
                    <Dialog.Actions>
                        <Button onPress={() => {
                            hideDialog()
                        }}>
                            <Text>Done</Text>
                        </Button>
                    </Dialog.Actions>
                </Dialog>
            </ImageBackground>
        </Container>
    )
}
export default AddCollection;

const styles = StyleSheet.create({
    background: {
        flex: 1,
    },
    container: {
        backgroundColor: 'white',
        margin: 24,
        marginTop: 16,
        height: '75%'
    },
    header: {
        backgroundColor: 'white',
        paddingTop:20
    },
    image: {
        flex: 1,
        marginTop: '5%'
    }
})
