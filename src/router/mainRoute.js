import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../screens/home'
import SearchScreen from '../screens/search';
import ProfileScreen from '../screens/profile';
import AddCollection from '../screens/addCollection';
import ViewCollection from '../screens/viewCollection';
import LoginScreen from '../screens/login';
import LoginForm from '../screens/loginScreen';
import RegisterScreen from '../screens/register';
import AddItems from '../screens/addItem';
import MyCollectionScreen from '../screens/myCollection';
import ResetPassword from '../screens/resetPassword';
const Stack = createStackNavigator();

function MainRoute() {
    return (
        <NavigationContainer>
            <Stack.Navigator
                initialRouteName="Home"
                screenOptions={{
                    headerShown: false
                }}
            >
                <Stack.Screen name="Home" component={HomeScreen} />
                <Stack.Screen name="Search" component={SearchScreen} />
                <Stack.Screen name="Profile" component={ProfileScreen} />
                <Stack.Screen name="AddCollection" component={AddCollection}/>
                <Stack.Screen name="AddItems" component={AddItems}/>
                <Stack.Screen name="ViewCollection" component={ViewCollection}/>
                <Stack.Screen name="Login" component={LoginScreen} />
                <Stack.Screen name="LoginForm" component={LoginForm}/>
                <Stack.Screen name="Register" component={RegisterScreen}/>
                <Stack.Screen name="MyCollection" component={MyCollectionScreen}/>
                <Stack.Screen name="ResetPassword" component={ResetPassword}/>
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default MainRoute;