export const LOGIN= 'LOGIN'

export const isLogin = (token) => {
    return { 
        type: LOGIN,
        token: token
    }
}

export const LOGOUT= 'LOGOUT'

export const isLogOut = () => {
    return { 
        type: LOGOUT,
    }
}
