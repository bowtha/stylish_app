import axios from 'axios'
export const host = 'https://stylish.chickenkiller.com'

export function getRandomCollection() {
    return axios.get(`${host}/randomCollection`)
}

export function login(data) {
    return axios.post(`${host}/login`,data)
}

export function signup(fromData) {
    return axios.post(`${host}/signup`,fromData)
}

export function getCollectionByFilter(token,search,fromData) {
    return axios.post(`${host}/collectionByFilter?Search=${search}`,fromData,{
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

export function getItemsByFilter(token,search,fromData) {
    return axios.post(`${host}/itemsByFilter?search=${search}`,fromData,{
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

export function getCollectionByID(token,id) {
    return axios.get(`${host}/collection/${id}`,{
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

export function getCollectionByUserID(token) {
    return axios.get(`${host}/collectionByUser`,{
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

export function updateUsername(token,data) {
    return axios.patch(`${host}/username`,data,{
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

export function postCollection(token,data) {
    return axios.post(`${host}/createCollection`,data,{
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

export function postItem(token,collection_id,fromData) {
    return axios.post(`${host}/createItem/${collection_id}`,fromData,{
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

export function getUserData(token) {
    return axios.get(`${host}/user`,{
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

export function logout(token) {
    return axios.patch(`${host}/logout`,{},{
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

export function resetPassword(token,password) {
    return axios.patch(`${host}/resetPassword`,password,{
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

export function getTempByCity(city){
    return axios.get(`https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=5dfe0bb00731f48c09ef062ae241a918`)
}

export function getTempByLatLong(lat,lon){
    return axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=5dfe0bb00731f48c09ef062ae241a918`)
}

export function postImage(data){
    return axios.post(`https://api.cloudinary.com/v1_1/stylish/image/upload`,data)
}





	