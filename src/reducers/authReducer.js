import { LOGIN,LOGOUT } from '../actions/loginAction'

const initialState = {
    token: null
}

export const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN:
            return {
                ...state,
                token: action.token
            }
        case LOGOUT:
            return {
                // ...state,
                token: null
            }
        default:
            return state
    }
}
